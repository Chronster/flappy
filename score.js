Score = function() {
	var scoreCounter = 0;
	var score = new Array(); 
	var digit0 = $('<div class="img-score-zero"></div>');
	digit0.css('left', '132px');
	$('.container').append(digit0);
	score.push(digit0);

	this.reset = function() {
		scoreCounter = 0;
		for (var i=0; i<score.length; i++) {
			score[i].remove();
		}
		score.length = 0;
		score.push(digit0);
		$('.container').append(digit0);
		//redraw();
	}

	var drawSingleDigit = function(digit, position, length) {
		var pos = 132;
		if (length%2 != 0) {
			pos = ((((length-1)/2)-0.5)*24+144)-(24*position);
		} else {
			pos = (((length-2)/2)*24+144)-(24*position);
		}

		var newDigit;

		switch (digit) {
			case 0:
				newDigit = $('<div class="img-score-zero"></div>');
				break;
			case 1:
				newDigit = $('<div class="img-score-one"></div>');
				break;
			case 2:
				newDigit = $('<div class="img-score-two"></div>');
				break;
			case 3:
				newDigit = $('<div class="img-score-three"></div>');
				break;
			case 4:
				newDigit = $('<div class="img-score-four"></div>');
				break;
			case 5:
				newDigit = $('<div class="img-score-five"></div>');
				break;
			case 6:
				newDigit = $('<div class="img-score-six"></div>');
				break;
			case 7:
				newDigit = $('<div class="img-score-seven"></div>');
				break;
			case 8:
				newDigit = $('<div class="img-score-eight"></div>');
				break;
			case 9:
				newDigit = $('<div class="img-score-nine"></div>');
				break;
		}

		if(position+1 > score.length) {
			//push
			score.push(newDigit);
			newDigit.css('left', (pos) + 'px');
			$('.container').append(newDigit);
		} else {
			//check if same and change if neccesary
			if (score[position].attr('class') != newDigit.attr('class')) {
				score[position].remove();
				score[position] = newDigit;
				newDigit.css('left', (pos) + 'px');
				$('.container').append(newDigit);
			}
		}
	}

	var redraw = function() {
		var digits = (scoreCounter.toString().split('')).reverse();
		
		for (var i=0; i<digits.length; i++) {
			drawSingleDigit(parseInt(digits[i]), i, digits.length)
		}
	}

	this.increase = function() {
		++scoreCounter;
		redraw();
	}
}