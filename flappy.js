$().ready(function() {
	var isRunning = false;
	var hasFlashed = true;
	var pos = 9999;
	var rot = 0;
	var velocity = 0;
	var gravity = 0.09;
	var flapImpulse = -5;
	//var groundBgPosMax = -586;
	var groundBgPosMax = -584;
	var groundBgPos = groundBgPosMax;
	//var groundBgPosMin = -632;
	var groundBgPosMin = -632;
	var scrollSpeed = 0.145;

	var pipe1 = new Obstacle(510);
	var pipe2 = new Obstacle(680);

	var score = new Score();
	var flash = $('.flash');
	var startBtn = $('.img-play');

	var date = Date.now();
	var start = date;
	var lastFlap = date;

	startBtn.mouseup(function() {
  		isRunning = true;
  		hasFlashed = false;
  		pos = 0;
  		rot = 0;
  		velocity = 0;
  		groundBgPos = groundBgPosMax;
  		pipe1.reset();
  		pipe2.reset();
  		score.reset();
  		startBtn.hide();
	});

	document.addEventListener('keydown', function(event) {
    	if(event.keyCode == 32) {
    		var now = Date.now();
    		if(isRunning && now - lastFlap >= 200) {
        		velocity = flapImpulse;
        		lastFlap = now;
        	}
    	}
	});

	var update = function() {
		
		var now = Date.now();
		var deltaTime = now - date;
		var bird = $('.img-flappy');
		var ground = $('.img-ground');
		//pos += deltaTime / 10;
		velocity = gravity * (deltaTime / 10) + velocity;
		pos += velocity;
		rot = velocity*10;
		if (rot > 65) {
			rot = 65;
		} else if (rot < (-65)) {
			rot = -65;
		}
		if (isRunning == true) {
			bird.css('-webkit-transform', 'translate(0, ' + (pos) + 'px) ' + 'rotate(' + (rot) + 'deg)');
			ground.css('background-position', (groundBgPos) + 'px 0px');
			//ground.css('-webkit-transform', 'translate(' + (groundBgPos) + 'px, 0px)');

			if (groundBgPos <= groundBgPosMin) {
				groundBgPos = groundBgPosMax;
			} else {
				groundBgPos -= scrollSpeed * deltaTime;
			}

			pipe1.move(scrollSpeed, deltaTime);
			pipe2.move(scrollSpeed, deltaTime);

			isRunning = pipe1.handleColissions(score);
			if (isRunning) {
				isRunning = pipe2.handleColissions(score);
			}
			if (!isRunning) {
				velocity = 0;
			}
		} else {
			if (hasFlashed == false) {
				flash.css('opacity', '1.0');
				if (flash.css('opacity') >= 1.0) {
					hasFlashed = true;
				}
			} else {
				if (flash.css('opacity') > 0.0) {
					flash.css('opacity', '0.0');
				} else {
					startBtn.show();
					bird.css('-webkit-transform', 'translate(0, ' + (pos) + 'px) ' + 'rotate(' + (rot) + 'deg)');
				}
			}
		}

		date = now;
		
		if(now -start > 3000) {
			//var pipeUp1 = $('<div class="img-pipe-hole-up"></div>');
			//pipeUp.css('height', '100px');
			//$('.container').append(pipeUp1);
			//score.increase();
			start = now;
		}
		
		requestAnimationFrame(update);
	};
	requestAnimationFrame(update);
});