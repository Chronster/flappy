Obstacle = function(offsetRight) {
	var isInFront = true;
	var xPos = offsetRight;
	var offset = offsetRight;
	var xStartPos = 288;
	var lowerPos = 0;
	var upperPos = 0;
	var pipeUp = $('<div class="img-pipe-hole-up"></div>');
	var pipeDown = $('<div class="img-pipe-hole-down"></div>');
	pipeUp.css('-webkit-transform', 'translate(' + (xPos) + 'px, 0px)');
	pipeDown.css('-webkit-transform', 'translate(' + (xPos) + 'px, 0px)');
	$('.container').append(pipeUp);
	$('.container').append(pipeDown);

	var randomizePipePos = function() {
		upperPos = -(Math.floor((Math.random()*215)+0));						//Random Number between 0 and 215
		lowerPos = upperPos + 125 + 86 + (Math.floor((Math.random()*12)+0)); 
		pipeUp.css('-webkit-transform', 'translate(' + (xPos) + 'px, ' + (lowerPos) + 'px)');
		pipeDown.css('-webkit-transform', 'translate(' + (xPos) + 'px, ' + (upperPos) + 'px)');
	}

	this.move = function(scrollSpeed, deltaTime) {
		if (xPos > -52) {
			xPos -= scrollSpeed * deltaTime;
		} else {
			xPos = xStartPos;
			randomizePipePos();
			isInFront = true;
		}
		
		pipeUp.css('-webkit-transform', 'translate(' + (xPos) + 'px, ' + (lowerPos) + 'px)');
		pipeDown.css('-webkit-transform', 'translate(' + (xPos) + 'px, ' + (upperPos) + 'px)');
	}

	this.handleColissions = function(score) {
		var ground = $('.img-ground');
		var flappy = $('.img-flappy');
		flappy.left = flappy.offset().left;
		flappy.right = flappy.offset().left + flappy.width();
		flappy.top = flappy.offset().top;
		flappy.bottom = flappy.offset().top + flappy.height();

		if (flappy.left <= pipeDown.offset().left + pipeDown.width() && 
			pipeDown.offset().left <= flappy.right && 
			flappy.top <= pipeDown.offset().top + pipeDown.height() && 
			pipeDown.offset().top <= flappy.bottom) {
			return false;
		}

		if (flappy.left <= pipeUp.offset().left + pipeUp.width() && 
			pipeUp.offset().left <= flappy.right && 
			flappy.top <= pipeUp.offset().top + pipeUp.height() && 
			pipeUp.offset().top <= flappy.bottom) {
			return false;
		}

		if (flappy.bottom >= ground.offset().top) {
			return false;
		}

		if (flappy.left > pipeUp.offset().left + pipeUp.width() && isInFront) {
			isInFront = false;
			score.increase();
		}

		return true;
	}

	this.reset = function() {
		isInFront = true;
		xPos = offset;
		randomizePipePos();
	}

	randomizePipePos();
}